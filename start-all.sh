cd devops_sample
mvn clean install
cd ..

if [ -d artifacts ]
then
    rm -rf artifacts
fi

mkdir artifacts

cp ./devops_sample/target/assignment*.jar ./artifacts

docker build -t u764/test .
docker push u764/test

kubectl apply -f db-secrets.yml
kubectl apply -f db-deployment.yml

while [ $(kubectl get pods -l app=db-service -o 'jsonpath={..status.conditions[?(@.type=="Ready")].status}') != "True" ]; do echo "waiting for pod" && sleep 1; done

sed -i "s~{{IMAGE_NAME}}~u764/test~g" sample/values.yaml

kubectl create configmap mysql-config --from-literal=MYSQL_URL="jdbc:mysql://$(minikube service mysql --url | cut -c8-)/sample?useSSL=false&allowPublicKeyRetrieval=true"

helm init
helm install sample --name=sample