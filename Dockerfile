FROM openjdk:14

#ENV SERVER_PORT=8090
#ENV H2_DATABASE_URL=jdbc:h2:mem:assignment

ENV SERVER_PORT=8090
ENV MYSQL_DATABASE_URL=jdbc:mysql://mysql/sample?allowPublicKeyRetrieval=true
ENV MYSQL_USER=root
ENV MYSQL_ROOT_PASSWORD=P@ssw0rd

RUN mkdir /app
ADD ./artifacts/*.jar /app/server.jar
ADD run.sh /run.sh
RUN chmod a+rwx /run.sh

ENTRYPOINT ["/run.sh"]
